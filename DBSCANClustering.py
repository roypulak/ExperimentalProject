# import necessary modules
import pandas as pd, numpy as np, matplotlib.pyplot as plt, time
from sklearn.cluster import DBSCAN
from geopy.distance import great_circle
from shapely.geometry import MultiPoint
from datetime import datetime as dt

# load the full location history json file downloaded from google
df_gps = pd.read_csv('data/unique_pic_1km.csv')
print('There are {:,} rows'.format(len(df_gps)))

# define the number of kilometers in one radian
kms_per_radian = 6371.0088

#df_gps = df_gps.iloc[range(0, len(df_gps), int(len(df_gps)/5000))] #uncomment to cluster only a sample


def get_centermost_point(cluster):
    centroid = (MultiPoint(cluster).centroid.x, MultiPoint(cluster).centroid.y)
    centermost_point = min(cluster, key=lambda point: great_circle(point, centroid).m)
    return tuple(centermost_point)


def dbscan_reduce(df, epsilon, x='lon', y='lat'):
    start_time = time.time()
    # represent points consistently as (lat, lon) and convert to radians to fit using haversine metric
    coords = df.as_matrix(columns=[y, x])
    db = DBSCAN(eps=epsilon, min_samples=25, algorithm='ball_tree', metric='haversine').fit(np.radians(coords))
    cluster_labels = db.labels_
    num_clusters = len(set(cluster_labels))
    print('Number of clusters: {:,}'.format(num_clusters))

    clusters = pd.Series([coords[cluster_labels==n] for n in range(num_clusters)])

    # find the point in each cluster that is closest to its centroid
    centermost_points = clusters.map(get_centermost_point)

    # unzip the list of centermost points (lat, lon) tuples into separate lat and lon lists
    lats, lons = zip(*centermost_points)
    rep_points = pd.DataFrame({x:lons, y:lats})
    rep_points.tail()

    # pull row from original data set where lat/lon match the lat/lon of each row of representative points
    rs = rep_points.apply(lambda row: df[(df[y]==row[y]) & (df[x]==row[x])].iloc[0], axis=1)

    # all done, print outcome
    message = 'Clustered {:,} points down to {:,} points, for {:.2f}% compression in {:,.2f} seconds.'
    print(message.format(len(df), len(rs), 100*(1 - float(len(rs)) / len(df)), time.time()-start_time))
    return rs



# first cluster the full gps location history data set coarsely, with epsilon=5km in radians
eps_rad = 1.0 / kms_per_radian
df_clustered = dbscan_reduce(df_gps, epsilon=eps_rad)
df_clustered.to_csv('data/clustered.csv', index=False, encoding='utf-8')
